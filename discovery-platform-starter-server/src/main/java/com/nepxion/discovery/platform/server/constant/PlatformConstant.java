package com.nepxion.discovery.platform.server.constant;

/**
 * <p>Title: Nepxion Discovery</p>
 * <p>Description: Nepxion Discovery</p>
 * <p>Copyright: Copyright (c) 2017-2050</p>
 * <p>Company: Nepxion</p>
 *
 * @author Ning Zhang
 * @version 1.0
 */

public class PlatformConstant {
    public static final String PLATFORM_VERSION = "1.0.0-SNAPSHOT";
    public static final String BASE_PACKAGE_NAME = "com.nepxion.discovery.platform.server";
    public static final String CURRENT_ADMIN_LOGIN = "CURRENT_ADMIN_LOGIN";
    public static final String DEFAULT_ADMIN_PASSWORD = "admin";
}