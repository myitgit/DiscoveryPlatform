package com.nepxion.discovery.platform.client.constant;

/**
 * <p>Title: Nepxion Discovery</p>
 * <p>Description: Nepxion Discovery</p>
 * <p>Copyright: Copyright (c) 2017-2050</p>
 * <p>Company: Nepxion</p>
 * @author Haojun Ren
 * @version 1.0
 */

public class PlatformConstant {
    public static final String GROUP = "nepxion";
    
    public static final String GATEWAY_ROUTE_DESCRIPTION = "Gateway dynamic route";
    public static final String ZUUL_ROUTE_DESCRIPTION = "Zuul dynamic route";
}